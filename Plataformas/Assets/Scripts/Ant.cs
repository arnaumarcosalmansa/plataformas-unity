﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ant : Character
{
    public int direction = 1;
    public float speed = 1;

    // Start is called before the first frame update

    void Start()
    {
        base.Start();
        rb.velocity = new Vector2(speed * direction, 0);

        base.orientationCorrectionMultiplier = -1;
    }

    // Update is called once per frame
    void Update()
    {
        if (!health.dead && !SceneDirector.instance.freeze)
        {
            rb.velocity = new Vector2(speed * direction, 0);
        }
        else
        {
            rb.velocity = Vector2.zero;
        }

        base.Update();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.gameObject.CompareTag("ground"))
        {
            direction *= -1;
            rb.velocity = new Vector2(speed * direction, 0);
        }

        if (collision.gameObject.CompareTag("Player"))
        {
            Health health = collision.gameObject.GetComponent<Health>();
            health.damage(1);
        }
    }
}
