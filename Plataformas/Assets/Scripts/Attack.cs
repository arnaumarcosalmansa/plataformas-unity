﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    private float lastAttack;
    public SpriteRenderer sprite;
    public Transform transform;

    public Sprite[] attackSprites;

    public float attackRate = 1;
    public float areaOffset = 0.1f;

    public AudioClip attackSound;
    private AudioSource audio;

    [HideInInspector]
    public int orientation = 1;
    // Start is called before the first frame update
    void Start()
    {
        lastAttack = 0;
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        bool attack = Input.GetAxis("Fire1") > 0;

        if(Time.time - attackRate >=  lastAttack && this.transform.rotation == Quaternion.identity && attack)
        {
            this.attack();
        }
    }

    private void attack()
    {
        Vector2 offset = new Vector2(areaOffset * orientation, 0);
        Collider2D[] colls = Physics2D.OverlapBoxAll((Vector2) this.transform.position + offset, new Vector2(0.15f, 0.2f), 0);

        foreach (Collider2D coll in colls)
        {
            if (coll.gameObject.CompareTag("enemy"))
            {
                Health health = coll.gameObject.GetComponent<Health>();
                health.damage(1);
            }
        }

        audio.clip = attackSound;
        audio.Play();

        transform.localPosition = new Vector2(areaOffset * orientation, transform.localPosition.y);
        sprite.flipX = orientation < 0;
        StartCoroutine(attackAnimation());
        lastAttack = Time.time;
    }

    private IEnumerator attackAnimation()
    {
        sprite.sprite = null;
        for (int i = 0; i < attackSprites.Length; i += 2)
        {
            sprite.sprite = attackSprites[i];
            yield return new WaitForSeconds(0.005f);
        }

        yield return new WaitForSeconds(0.1f);
        sprite.sprite = null;
    }
}
