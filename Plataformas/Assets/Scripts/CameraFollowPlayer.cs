﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour
{
    public Transform player;

    public Vector2 offset = Vector2.zero;
    // Start is called before the first frame update

    private Vector3 v3zero = Vector3.zero;
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (player == null)
        {
            return;
        }

        Vector3 destination = new Vector3(player.position.x + offset.x, player.position.y + offset.y, transform.position.z);

        transform.position = Vector3.SmoothDamp(transform.position, destination, ref v3zero, 0.2f);
    }
}
