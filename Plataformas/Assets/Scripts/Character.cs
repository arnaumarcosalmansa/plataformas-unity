﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    public Health health;
    protected Animator animator;
    protected SpriteRenderer spriteRenderer;
    protected Rigidbody2D rb;
    protected Collider2D[] colliders;
    protected AudioSource audio;

    public int orientationCorrectionMultiplier = 1;
    public AudioClip audioOnDamage;

    public void Start()
    {
        health = GetComponent<Health>();
        health.character = this;

        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        colliders = GetComponents<Collider2D>();
        rb = GetComponent<Rigidbody2D>();
        audio = GetComponent<AudioSource>();

        //Debug.Log("colliders: " + colliders.Length);
    }

    public void Update()
    {
        if (Mathf.Abs(rb.velocity.x) > 0.05f)
        {
            if (rb.velocity.x * orientationCorrectionMultiplier < 0)
            {
                spriteRenderer.flipX = true;
            }
            else
            {
                spriteRenderer.flipX = false;
            }
        }
    }

    IEnumerator flash()
    {
        spriteRenderer.material.SetFloat("_FlashAmount", 1);
        yield return new WaitForSeconds(0.1f);
        spriteRenderer.material.SetFloat("_FlashAmount", 0);
        yield return new WaitForSeconds(0.075f);
        spriteRenderer.material.SetFloat("_FlashAmount", 1);
        yield return new WaitForSeconds(0.05f);
        spriteRenderer.material.SetFloat("_FlashAmount", 0);

        SceneDirector.instance.freeze = false;
    }

    virtual public void die()
    {
        foreach (Collider2D c in colliders)
        {
            c.enabled = false;
        }
        rb.isKinematic = true;

        animator.Play("die");
        float length = 48f / 60f;
        Destroy(this.gameObject, length);
    }

    virtual public void damage()
    {
        audio.clip = audioOnDamage;
        audio.Play();
        SceneDirector.instance.freeze = true;
        StartCoroutine(flash());
    }
}
