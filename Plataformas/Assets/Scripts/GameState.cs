﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoBehaviour
{
    public Player player;

    public GameObject finishMenu;

    public TMPro.TextMeshProUGUI title;

    private bool triggered = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!triggered && player.health.dead)
        {
            finishMenu.SetActive(true);
            GameFail();
            triggered = true;
        }

        if (player.won)
        {
            finishMenu.SetActive(true);
            GameWin();
            triggered = true;
            Time.timeScale = 0f;
        }
    }

    void GameFail()
    {
        title.text = "You Died!";
    }

    void GameWin()
    {
        string message = "You Won!";
        if (Score.HighScore())
        {
            message += "\nNew Record!!!";
        }
        title.text = message;
    }
}
