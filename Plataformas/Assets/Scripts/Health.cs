﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int maxHealth;
    public float invulneravilityTimeAfterHit = 1f;
    public int pointsOnDeath;

    [HideInInspector]
    public int currentHealth;

    private SpriteRenderer sprite;
    private Animator animator;
    [HideInInspector]
    public Character character;

    private float lastHit;

    public bool fullHealth
    {
        get { return this.currentHealth == this.maxHealth;  }
    }

    public bool dead
    {
        get { return this.currentHealth <= 0;  }
    }

    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();

        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void damage(int amount)
    {
        if (lastHit + invulneravilityTimeAfterHit > Time.time)
        {
            return;
        }

        this.currentHealth -= amount;
        character.damage();
        lastHit = Time.time;
        if (this.currentHealth <= 0)
        {
            character.die();
            Score.add(pointsOnDeath);
        }
    }

    public void heal(int amount)
    {
        this.currentHealth += amount;
        this.currentHealth = Mathf.Min(this.currentHealth, this.maxHealth);
    }

    virtual protected void die()
    {
        foreach (Collider c in GetComponents<Collider>())
        {
            c.enabled = false;
        }

        Destroy(this.gameObject, 1);
        animator.Play("die");
    }
}
