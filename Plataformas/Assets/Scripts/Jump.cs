﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    public float jumpVelocity;

    [SerializeField] private LayerMask groundMask;
    [SerializeField] private Transform groundCheckTransform;

    private Rigidbody2D rb;
    private Animator animator;

    private bool grounded = true;
    public bool hasPermissionToJump = true;
    const float groundedRadius = .05f;

    private bool jumpAxisInUse = false;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (hasPermissionToJump && grounded && !jumpAxisInUse && Input.GetAxisRaw("Jump") > 0)
        {
            jumpAxisInUse = true;
            rb.velocity = jumpVelocity * Vector2.up;
            animator.SetBool("jumping", true);

            //rb.constraints = RigidbodyConstraints2D.None;
        }

        if (Input.GetAxis("Jump") == 0)
        {
            jumpAxisInUse = false;
        }
    }

    void FixedUpdate()
    {
        grounded = false;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheckTransform.position, groundedRadius, groundMask);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                grounded = true;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("ground"))
        {
            animator.SetBool("jumping", false);
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        }

        //animator.SetBool("jumping", false);
        this.transform.rotation = Quaternion.identity;
    }
}
