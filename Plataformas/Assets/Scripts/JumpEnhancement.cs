﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpEnhancement : MonoBehaviour
{
    public float fallVelocityMultiplier;
    public float lowJumpMultiplier;

    private Rigidbody2D rb;
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallVelocityMultiplier - 1) * Time.deltaTime;
        }
        else if (rb.velocity.y > 0 && !(Input.GetAxisRaw("Jump") == 1))
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }
    }
}
