﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float horizontalSpeed;

    private Rigidbody2D rb;
    private Animator animator;
    private SpriteRenderer sprite;
    private Health health;

    [HideInInspector]
    public float slowMultiplier = 1f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        health = GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
        if (health.dead || SceneDirector.instance.freeze)
        {
            rb.velocity = Vector2.zero;
            return;
        }

        float multiplier = Input.GetAxis("Horizontal");

        rb.velocity = new Vector2(multiplier * horizontalSpeed * slowMultiplier, rb.velocity.y);

        animator.SetFloat("horizontalSpeed", Mathf.Abs(rb.velocity.x));
    }
}
