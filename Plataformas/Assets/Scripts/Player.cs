﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    private Movement movement;
    private Jump jump;
    private Attack attack;

    [HideInInspector]
    public bool won = false;

    // Start is called before the first frame update
    void Start()
    {
        base.Start();
        movement = GetComponent<Movement>();
        jump = GetComponent<Jump>();
        attack = GetComponent<Attack>();
    }

    // Update is called once per frame
    void Update()
    {
        bool slow = Input.GetAxis("Slow") > 0;

        if (!slow && Mathf.Abs(rb.velocity.x) > 0.025f)
        {
            if (rb.velocity.x * orientationCorrectionMultiplier < 0)
            {
                spriteRenderer.flipX = true;
                attack.orientation = -1;
            }
            else
            {
                spriteRenderer.flipX = false;
                attack.orientation = 1;
            }
        }

        if (slow)
        {
            movement.slowMultiplier = 0.2f;
            jump.hasPermissionToJump = false;
            animator.speed = 0.33f;
        }
        else
        {
            movement.slowMultiplier = 1f;
            jump.hasPermissionToJump = true;
            animator.speed = 1f;
        }
    }

    override public void die()
    {
        base.die();
    }

    override public void damage()
    {
        base.damage();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Finish"))
        {
            this.won = true;
        }
    }
}
