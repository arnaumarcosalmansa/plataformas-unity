﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedAnt : Character
{
    public int direction = 1;
    public float speed = 1;

    public float range = 1;

    private float startingX;

    // Start is called before the first frame update
    private float lastRangeCheck;
    private float timeToCheckRange = 0.5f; 

    void Start()
    {
        base.Start();
        rb.velocity = new Vector2(speed * direction, 0);

        base.orientationCorrectionMultiplier = -1;

        startingX = transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        if (!health.dead && !SceneDirector.instance.freeze)
        {
            rb.velocity = new Vector2(speed * direction, 0);
        }
        else
        {
            rb.velocity = Vector2.zero;
        }

        if (lastRangeCheck + timeToCheckRange < Time.time && Mathf.Abs(transform.position.x - startingX) >= range)
        {
            direction = -direction;
            lastRangeCheck = Time.time;
        }

        base.Update();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Health health = collision.gameObject.GetComponent<Health>();
            health.damage(1);
        }
    }
}
