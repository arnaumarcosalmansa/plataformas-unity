﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneDirector
{
    private static SceneDirector _instance { get; set; }

    public static SceneDirector instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new SceneDirector();
            }
            return _instance;
        }
    }

    public bool freeze = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
