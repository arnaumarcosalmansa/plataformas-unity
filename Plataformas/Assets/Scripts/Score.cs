﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Score: MonoBehaviour
{
    public static int points;

    public string scoreText;
    //public Text text;
    //public TextMesh text;
    public TMPro.TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
        points = 0;
    }

    // Update is called once per frame
    void Update()
    {
        text.text = scoreText + points;
    }

    public static void add(int amount)
    {
        points += amount;
    }

    public static bool HighScore()
    {
        int maxScore = PlayerPrefs.GetInt("highscore", 0);
        bool record = points > maxScore;

        if (record)
        {
            PlayerPrefs.SetInt("highscore", points);
            PlayerPrefs.Save();
        }

        return record;
    }
}
