﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class Settings : MonoBehaviour
{
    public Slider volumeSlider;
    public Slider gameSpeedSlider;

    public AudioMixer mixer;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = PlayerPrefs.GetFloat("game_speed", 1f);
        if (mixer != null)
        {
            float volume = PlayerPrefs.GetFloat("volume_multiplier", 1f);
            mixer.SetFloat("volume", volume);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SaveSettings()
    {
        float volume = volumeSlider.value;
        PlayerPrefs.SetFloat("volume_multiplier", volume);

        float gameSpeed = gameSpeedSlider.value / gameSpeedSlider.maxValue;
        PlayerPrefs.SetFloat("game_speed", gameSpeed);
        PlayerPrefs.Save();
    }

    public void FillSliders()
    {
        volumeSlider.value = PlayerPrefs.GetFloat("volume_multiplier", volumeSlider.maxValue);
        gameSpeedSlider.value = PlayerPrefs.GetFloat("game_speed", gameSpeedSlider.maxValue);
    }
}
